package com.kshrd.recyclerview;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import java.util.ArrayList;
import java.util.Arrays;

public class MainActivity extends AppCompatActivity {
    ArrayList<String> title = new ArrayList<>(Arrays.asList("Title One", "Title Two", "Title Three", "Title Four", "Title Five", "Title Six", "Title Seven"));
    ArrayList<String> content = new ArrayList<>(Arrays.asList("Content 1 Bla Bla Bla", "Content 2 Bla Bla Bla", "Content 3 Bla Bla Bla", "Content 4 Bla Bla Bla", "Content 5 Bla Bla Bla", "Content 6 Bla Bla Bla", "Content 7 Bla Bla Bla"));

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        RecyclerView recyclerView = findViewById(R.id.recyclerView);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(linearLayoutManager);
        CustomAdapter customAdapter = new CustomAdapter(title, content, MainActivity.this);
        recyclerView.setAdapter(customAdapter);
    }
}